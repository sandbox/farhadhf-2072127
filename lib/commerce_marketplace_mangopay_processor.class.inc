<?php

/**
 * @file
 * MangoPay funds processor class.
 */

class CommerceMarketplaceMangoPayProcessor implements CommerceMarketplaceFundsProcessorInterface {
  public function createAccount(&$values) {
    $values += array(
      'mango_user_id' => array(),
    );
  }

  public function saveAccount($account) {
    if (!isset($account->mango_user_id[LANGUAGE_NONE][0]['value']) || empty($account->mango_user_id[LANGUAGE_NONE][0]['value'])) {
      $mango_user = commerce_marketplace_mangopay_create_mango_user($account->owner_type ,$account->owner_id, "User for store $account->owner_id");
      if (isset($mango_user) && isset($mango_user->ID)) {
        $account->mango_user_id[LANGUAGE_NONE][0]['value'] = $mango_user->ID;
        watchdog('commerce_marketplace_mangopay', 'user created with user id : @user', array('@user' => $mango_user->ID));
      }
    }
  }

  public function createTransaction(&$values) {
    $values += array(
      'payer_id' => '',
      'beneficiary_id' => '',
    );
  }

  public function saveTransaction($transaction) {
    if ($transaction->type != 'deposit' && $transaction->type != 'withdraw') {
      if (empty($transaction->payer_id) && empty($transaction->mango_transaction_id)) {
        $wallet_id = 0;
        if ($transaction->type == 'payment' && empty($transaction->from_account)) {
          $payer_id = variable_get('commerce_marketplace_mangopay_global_user_id');
          $wallet_id = variable_get('commerce_marketplace_mangopay_global_wallet_id');
        }
        else {
          $payer_account = commerce_marketplace_funds_account_load($transaction->from_account);
          $payer_id = commerce_marketplace_mangopay_mango_user_load($payer_account);
        }
        $to_account = commerce_marketplace_funds_account_load($transaction->to_account);
        $ben_id = commerce_marketplace_mangopay_mango_user_load($to_account);
        $amount = $transaction->amount[LANGUAGE_NONE][0]['amount'];
        $tag = 'Commerce marketplace mangopay transaction';
        $transfer = commerce_marketplace_mangopay_transfer_money($payer_id, $wallet_id, $ben_id, 0, $amount, $tag);
        if (isset($transfer) && isset($transfer->ID)) {
          $transaction->payer_id[LANGUAGE_NONE][0]['value'] = $payer_id;
          $transaction->beneficiary_id[LANGUAGE_NONE][0]['value'] = $ben_id;
          $transaction->mango_transaction_id[LANGUAGE_NONE][0]['value'] = $transaction->ID;
        }
      }
    }
    if ($transaction->type == 'withdraw' && $transaction->status == 'success' && empty($transaction->mango_transaction_id)) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'cmp_funds_withdraw_method')
        ->entityCondition('bundle', 'mangopay')
        ->propertyCondition('account_id', $transaction->from_account);
      $result = $query->execute();
      $methods = $result['cmp_funds_withdraw_method'];
      $beneficiary = 0;
      $amount = $transaction->amount[LANGUAGE_NONE][0]['amount'];
      $from_account = commerce_marketplace_funds_account_load($transaction->from_account);
      $mango_user_id = commerce_marketplace_mangopay_mango_user_load($from_account);
      foreach ($methods as $method_id => $method) {
        $beneficiary = commerce_marketplace_funds_withdraw_method_load($method_id);
        break;
      }
      $beneficiary_id = $beneficiary->mango_beneficiary_id[LANGUAGE_NONE][0]['value'];
      $body = array(
        'UserID' => $mango_user_id,
        'WalletID' => 0,
        'BeneficiaryID' => $beneficiary_id,
        'Amount' => $amount,
        'Tag' => 'Withdraw for account id = ' . $transaction->from_account,
      );
      $body = json_encode($body);
      $withdraw = commerce_marketplace_mangopay_request('withdrawals', 'POST', $body);
      if (isset($withdraw->ID)) {
        $transaction->mango_transaction_id[LANGUAGE_NONE][0]['value'] = $withdraw->ID;
      }
      else {
        // @todo : later
      }
    }
  }

  public function createWithdrawMethod(&$values) {
    $values += array(
      'mango_bank_account_owner_name',
      'mango_bank_account_owner_address',
      'mango_bank_account_iban',
      'mango_bank_account_bic',
      'mango_beneficiary_id',
    );
  }

  public function saveWithdrawMethod($method) {
    if ($method->type == 'mangopay' && empty($method->mango_beneficiary_id[LANGUAGE_NONE][0]['value'])) {
      $owner_id = $method->account_id;
      $owner = commerce_marketplace_funds_account_load($owner_id);
      $body = array(
        'BankAccountOwnerName' => $method->mango_bank_account_owner_name[LANGUAGE_NONE][0]['value'],
        'BankAccountOwnerAddress' => $method->mango_bank_account_owner_address[LANGUAGE_NONE][0]['value'],
        'BankAccountIBAN' => $method->mango_bank_account_iban[LANGUAGE_NONE][0]['value'],
        'BankAccountBIC' => $method->mango_bank_account_bic[LANGUAGE_NONE][0]['value'],
        'UserID' => $owner->mango_user_id[LANGUAGE_NONE][0]['value'],
        'Tag' => 'Withdraw method for account ' . $owner_id,
      );
      $body = json_encode($body);
      $beneficiary = commerce_marketplace_mangopay_request("beneficiaries", "POST", $body);
      watchdog('mango' , 'beneficiary result !result', array('!result' => '<pre>' . var_export($beneficiary, TRUE) . '</pre>'));
      if (isset($beneficiary->ID)) {
        $method->mango_beneficiary_id[LANGUAGE_NONE][0]['value'] = $beneficiary->ID;
      }
      else {
        drupal_set_message('Error with creating withdraw method', 'error');
      }
    }
  }
}
