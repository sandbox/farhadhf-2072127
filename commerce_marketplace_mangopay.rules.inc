<?php

/**
 * Rules integration for commerce_marketplace mangopay integration.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_marketplace_mangopay_rules_condition_info() {
  $conditions = array();

  $conditions['user_registered_card'] = array(
    'label' => t('User registered card on mangopay'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce Marketplace Mangopay'),
    'callbacks' => array(
      'execute' => 'commerce_marketplace_mangopay_user_registered_card',
    ),
  );

  return $conditions;
}

function commerce_marketplace_mangopay_user_registered_card($order) {
  if ($order->type == 'cmp_funds_deposit_order') {
    $transaction_id = db_select('commerce_marketplace_funds_transaction', 't')
      ->condition('type', 'deposit')
      ->condition('order_id', $order->order_id)
      ->fields('t', array('transaction_id'))
      ->execute()->fetchField();
    $transaction = commerce_marketplace_funds_transaction_load($transaction_id);
    $account = commerce_marketplace_funds_account_load($transaction->to_account);
    $UserID = commerce_marketplace_mangopay_mango_user_load($account);
  }
  else {
    $payer_uid = $order->uid;
    $payer_account = commerce_marketplace_funds_account_load_by_owner('user', $payer_uid);
    $UserID = commerce_marketplace_mangopay_mango_user_load($payer_account);
  }
  if (!empty($UserID)) {
    $cards = commerce_marketplace_mangopay_request('users/' . $UserID . '/cards', 'GET');
    if (empty($cards)) {
      return FALSE;
    }
    return TRUE;
  }
  return FALSE;
}
